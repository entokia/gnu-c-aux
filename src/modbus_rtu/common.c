#include "modbus_rtu/common.h"

#ifdef ENABLE_MODBUS_RTU_COMMON

#include "modbus_rtu/private.h"
#include "endianness.h"

int
modbus_rtu_request_length(uint8_t function, uint16_t data_count)
{
    if        (function == MODBUS_FUNC_READ_INPUT_REGISTERS
            || function == MODBUS_FUNC_READ_HOLDING_REGISTERS) {

        return 8;

    } else if (function == MODBUS_FUNC_READ_DISCRETE_INPUTS
            || function == MODBUS_FUNC_READ_COILS) {

        return 8;

    } else if (function == MODBUS_FUNC_WRITE_SINGLE_REGISTER
            || function == MODBUS_FUNC_WRITE_SINGLE_COIL) {

        return 8;

    } else if (function == MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS) {

        int length      = multiple_regs_data_length(data_count);
        return (length > 0) ? (length + 9) : length;

    } else if (function == MODBUS_FUNC_WRITE_MULTIPLE_COILS) {

        int length      = multiple_bits_data_length(data_count);
        return (length > 0) ? (length + 9) : length;

    }

    return MODBUS_ERROR_UNKNOWN_FUNCTION;
}

int
modbus_rtu_response_length(uint8_t function, uint16_t data_count)
{
    if (function & MODBUS_RESPONSE_FAULT)
        return 5;

    if        (function == MODBUS_FUNC_READ_INPUT_REGISTERS
            || function == MODBUS_FUNC_READ_HOLDING_REGISTERS) {

        int length      = multiple_regs_data_length(data_count);
        return (length > 0) ? (length + 5) : 0;

    } else if (function == MODBUS_FUNC_READ_DISCRETE_INPUTS
            || function == MODBUS_FUNC_READ_COILS) {

        int length      = multiple_bits_data_length(data_count);
        return (length > 0) ? (length + 5) : 0;

    } else if (function == MODBUS_FUNC_WRITE_SINGLE_REGISTER
            || function == MODBUS_FUNC_WRITE_SINGLE_COIL) {

        return 8;

    } else if (function == MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS
            || function == MODBUS_FUNC_WRITE_MULTIPLE_COILS) {

        return 8;

    }

    return MODBUS_ERROR_UNKNOWN_FUNCTION;
}

uint16_t
modbus_rtu_reg_address(const void *memory_ptr, const void *reg_ptr)
{
    return (uint16_t)((const uint16_t *)reg_ptr - (const uint16_t *)memory_ptr);
}

uint16_t
modbus_rtu_bit_address(const void *memory_ptr, const void *byte_ptr, uint8_t bit_number)
{
    return (uint16_t)(((const uint8_t *)byte_ptr - (const uint8_t *)memory_ptr) * 8 + bit_number);
}

uint8_t
modbus_rtu_get_bit(const uint8_t *memory_ptr, uint16_t memory_bit_offset, uint16_t bit_address)
{
    uint16_t offset     = bit_address - memory_bit_offset;
    return (uint8_t)((memory_ptr[offset / 8] >> (offset % 8)) & 0x01u);
}

void
modbus_rtu_set_bit(uint8_t *memory_ptr, uint16_t memory_bit_offset, uint16_t bit_address, uint8_t bit_value)
{
    uint16_t offset     = bit_address - memory_bit_offset;
    uint16_t id         = bit_address / 8;
    uint8_t bitMask     = (uint8_t)(0x01u << (offset % 8));
    if (bit_value)
        memory_ptr[id] |=  bitMask;
    else
        memory_ptr[id] &= ~bitMask;
}

uint16_t
modbus_rtu_ansi_crc16(const uint8_t *data, uint8_t length, uint8_t swap_bytes)
{
    if (!data || ! length)
        return 0;

    uint16_t sum        = 0xFFFFu;

    for (uint8_t i = 0; i < length; ++i) {
        uint8_t key     = (uint8_t)(sum ^ data[i]);
        sum             = (sum >> 8) ^ s_ansi_crc16_table[key];
    }

    if (swap_bytes)
        return byte_swap_16(sum);
    else
        return sum;
}

#endif // ENABLE_MODBUS_RTU_COMMON
