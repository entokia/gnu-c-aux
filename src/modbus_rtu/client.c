#include "modbus_rtu/client.h"

#ifdef ENABLE_MODBUS_RTU_CLIENT

#include "endianness.h"
#include "modbus_rtu/private.h"

int
modbus_rtu_make_read_coils_request(
        uint8_t device_address,
        uint16_t starting_address, uint16_t coil_count,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_READ_COILS, coil_count,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    DfaaccssPacket *p   = (DfaaccssPacket *)message_buffer;
    p->dev_addr         = device_address;
    p->function         = MODBUS_FUNC_READ_COILS;
    p->data_addr        = host_to_be16(starting_address);
    p->data_count       = host_to_be16(coil_count);
    p->checksum         = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_read_discrete_inputs_request(
        uint8_t device_address,
        uint16_t starting_address, uint16_t input_count,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_READ_DISCRETE_INPUTS, input_count,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    DfaaccssPacket *p   = (DfaaccssPacket *)message_buffer;
    p->dev_addr         = device_address;
    p->function         = MODBUS_FUNC_READ_DISCRETE_INPUTS;
    p->data_addr        = host_to_be16(starting_address);
    p->data_count       = host_to_be16(input_count);
    p->checksum         = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_read_holding_registers_request(
        uint8_t device_address,
        uint16_t starting_address, uint16_t register_count,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_READ_HOLDING_REGISTERS, register_count,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    DfaaccssPacket *p   = (DfaaccssPacket *)message_buffer;
    p->dev_addr         = device_address;
    p->function         = MODBUS_FUNC_READ_HOLDING_REGISTERS;
    p->data_addr        = host_to_be16(starting_address);
    p->data_count       = host_to_be16(register_count);
    p->checksum         = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_read_input_registers_request(
        uint8_t device_address,
        uint16_t starting_address, uint16_t register_count,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_READ_INPUT_REGISTERS, register_count,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    DfaaccssPacket *p   = (DfaaccssPacket *)message_buffer;
    p->dev_addr         = device_address;
    p->function         = MODBUS_FUNC_READ_INPUT_REGISTERS;
    p->data_addr        = host_to_be16(starting_address);
    p->data_count       = host_to_be16(register_count);
    p->checksum         = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_write_single_coil_request(
        uint8_t device_address,
        uint16_t coil_address, uint16_t coil_value,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_WRITE_SINGLE_COIL, 1,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    DfaavvssPacket *p   = (DfaavvssPacket *)message_buffer;
    p->dev_addr         = device_address;
    p->function         = MODBUS_FUNC_WRITE_SINGLE_COIL;
    p->data_addr        = host_to_be16(coil_address);
    p->data_value       = host_to_be16(coil_value ? 0xFF00u : 0x0000u);
    p->checksum         = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_write_signle_register_request(
        uint8_t device_address,
        uint16_t register_address, uint16_t register_value,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_WRITE_SINGLE_REGISTER, 1,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    DfaavvssPacket *p   = (DfaavvssPacket *)message_buffer;
    p->dev_addr         = device_address;
    p->function         = MODBUS_FUNC_WRITE_SINGLE_REGISTER;
    p->data_addr        = host_to_be16(register_address);
    p->data_value       = host_to_be16(register_value);
    p->checksum         = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_write_multiple_coils_request(
        uint8_t device_address,
        uint16_t starting_address, uint16_t coil_count,
        const void *memory_ptr, uint16_t memory_bit_offset, uint16_t memory_bit_size,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_WRITE_MULTIPLE_COILS, coil_count,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    int req_mem_size    = starting_address - memory_bit_offset + coil_count;
    if (!memory_ptr || starting_address < memory_bit_offset || memory_bit_size < req_mem_size)
        return MODBUS_ERROR_INSUFFICIENT_MEMORY_SIZE;

    DfaacclbHeader *h   = (DfaacclbHeader *)message_buffer;
    h->dev_addr         = device_address;
    h->function         = MODBUS_FUNC_WRITE_MULTIPLE_COILS;
    h->bit_addr         = host_to_be16(starting_address);
    h->bit_count        = host_to_be16(coil_count);
    h->byte_len         = (uint8_t)multiple_bits_data_length(coil_count);

    uint8_t *last_byte  = &message_buffer[length - 3];
    *last_byte          = 0;

    const uint8_t * m   = (const uint8_t *)memory_ptr;
    for (uint16_t i = 0; i < coil_count; ++i) {
        modbus_rtu_set_bit(&h->byte_data, 0, i,
                           modbus_rtu_get_bit(m, memory_bit_offset, starting_address + i));
    }

    uint16_t *crc       = (uint16_t *)(void *)&message_buffer[length - 2];
    *crc                = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_make_write_multiple_registers_request(
        uint8_t device_address,
        uint16_t starting_address, uint16_t register_count,
        const void *memory_ptr, uint16_t memory_reg_offset, uint16_t memory_reg_size,
        uint8_t *message_buffer, uint8_t buffer_length)
{
    int length          = check_message_buffer_and_calc_length(MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS, register_count,
                                                               message_buffer, buffer_length);
    if (length < 0)
        return length;

    int req_mem_size    = starting_address - memory_reg_offset + register_count;
    if (!memory_ptr || starting_address < memory_reg_offset || memory_reg_size < req_mem_size)
        return MODBUS_ERROR_INSUFFICIENT_MEMORY_SIZE;

    DfaacclwHeader *h   = (DfaacclwHeader *)message_buffer;
    h->dev_addr         = device_address;
    h->function         = MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS;
    h->reg_addr         = host_to_be16(starting_address);
    h->reg_count        = host_to_be16(register_count);
    h->byte_len         = (uint8_t)multiple_regs_data_length(register_count);

    uint16_t offset     = starting_address - memory_reg_offset;
    uint16_t *d         = &h->word_data;
    const uint16_t *m   = (const uint16_t *)memory_ptr;
    for (uint8_t i = 0; i < register_count; ++i) {
        d[i]            = host_to_be16(m[offset + i]);
    }

    uint16_t *crc       = (uint16_t *)(void *)&message_buffer[length - 2];
    *crc                = modbus_rtu_ansi_crc16(message_buffer, (uint8_t)(length - 2), 0);

    return length;
}

int
modbus_rtu_validate_response(
        const uint8_t *response, uint8_t response_length,
        const uint8_t *request, uint8_t request_length)
{
    // Validating buffers and minimal length
    if (!response || response_length < MIN_MESSAGE_LEN || !request || request_length < MIN_MESSAGE_LEN)
        return MODBUS_ERROR_INSUFFICIENT_BUFFER_SIZE;

    // Validating device address
    if (request[0] && response[0] != request[0])
        return MODBUS_ERROR_WRONG_DEVICE_ADDRESS;

    // Validating function
    uint8_t function    = response[1] & FUNC_MASK;
    if (function != request[1])
        return MODBUS_ERROR_WRONG_FUNCTION;

    // Detecting if response is a fault message
    uint8_t type        = response[1] & MODBUS_RESPONSE_FAULT;

    // Data count is filled depending on response type and function
    uint16_t data_count = 0;
    if (type == MODBUS_RESPONSE_NORMAL) {
        if        (function == MODBUS_FUNC_READ_INPUT_REGISTERS
                || function == MODBUS_FUNC_READ_HOLDING_REGISTERS
                || function == MODBUS_FUNC_READ_DISCRETE_INPUTS
                || function == MODBUS_FUNC_READ_COILS
                || function == MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS
                || function == MODBUS_FUNC_WRITE_MULTIPLE_COILS) {

            const DfaaccHeader *rq      = (const DfaaccHeader *)request;
            data_count                  = be16_to_host(rq->data_count);

        } else if (function == MODBUS_FUNC_WRITE_SINGLE_REGISTER
                || function == MODBUS_FUNC_WRITE_SINGLE_COIL) {

            data_count                  = 1;

        } else {

            return MODBUS_ERROR_UNKNOWN_FUNCTION;

        }
    }

    // Validating message length
    if (response_length != modbus_rtu_response_length(response[1], data_count))
        return MODBUS_ERROR_WRONG_MESSAGE_LENGTH;

    // Validating checksum
    const uint16_t *csm = (const uint16_t *)(const void *)&response[response_length - 2];
    uint16_t csc        = modbus_rtu_ansi_crc16(response, response_length - 2, 0);
    if (csc != *csm)
        return MODBUS_ERROR_WRONG_CHECKSUM;

    // Other checks depending on response type and function
    if (type == MODBUS_RESPONSE_NORMAL) {

        if        (function == MODBUS_FUNC_READ_INPUT_REGISTERS
                || function == MODBUS_FUNC_READ_HOLDING_REGISTERS) {

            const DflwHeader *rp        = (const DflwHeader *)response;

            // Validating data length
            if (rp->byte_len != multiple_regs_data_length(data_count))
                return MODBUS_ERROR_WRONG_BYTE_COUNT;

        } else if (function == MODBUS_FUNC_READ_DISCRETE_INPUTS
                || function == MODBUS_FUNC_READ_COILS) {

            const DflbHeader *rp        = (const DflbHeader *)response;

            // Validating data length
            if (rp->byte_len != multiple_bits_data_length(data_count))
                return MODBUS_ERROR_WRONG_BYTE_COUNT;

        } else if (function == MODBUS_FUNC_WRITE_SINGLE_REGISTER
                || function == MODBUS_FUNC_WRITE_SINGLE_COIL) {

            const DfaavvssPacket *rq    = (const DfaavvssPacket *)request;
            const DfaavvssPacket *rp    = (const DfaavvssPacket *)response;

            // Validating data address
            if (rp->data_addr != rq->data_addr)
                return MODBUS_ERROR_WRONG_DATA_ADDRESS;

            // Validating data value
            if (rp->data_value != rq->data_value)
                return MODBUS_ERROR_WRONG_DATA_VALUE;

        } else if (function == MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS
                || function == MODBUS_FUNC_WRITE_MULTIPLE_COILS) {

            const DfaaccHeader *rq      = (const DfaaccHeader *)request;
            const DfaaccssPacket *rp    = (const DfaaccssPacket *)response;

            // Validating data address
            if (rp->data_addr != rq->data_addr)
                return MODBUS_ERROR_WRONG_DATA_ADDRESS;

            // Validating data count
            if (rp->data_count != rq->data_count)
                return MODBUS_ERROR_WRONG_DATA_COUNT;

        }
    }

    return type;
}

int
modbus_rtu_get_fault_response_exception(
        const uint8_t *validated_response)
{
    const DfessPacket *p        = (const DfessPacket *)validated_response;
    return p->exception;
}

int
modbus_rtu_apply_normal_response_bits_to_memory(
        const uint8_t *validated_response,
        uint16_t starting_address, uint16_t bit_count,
        void *memory_ptr, uint16_t memory_bit_offset, uint8_t memory_bit_size)
{
    int req_mem_size    = starting_address - memory_bit_offset + bit_count;
    if (!memory_ptr || starting_address < memory_bit_offset || memory_bit_size < req_mem_size)
        return MODBUS_ERROR_INSUFFICIENT_MEMORY_SIZE;

    const uint8_t *d    = &((const DflbHeader *)validated_response)->byte_data;
    for (uint16_t i = 0; i < bit_count; ++i) {
        modbus_rtu_set_bit((uint8_t *)memory_ptr, memory_bit_offset, starting_address + i,
                           modbus_rtu_get_bit(d, 0, i));
    }

    return MODBUS_SUCCESS;
}

int
modbus_rtu_apply_normal_response_registers_to_memory(
        const uint8_t *validated_response,
        uint16_t starting_address, uint16_t register_count,
        void *memory_ptr, uint16_t memory_reg_offset, uint8_t memory_reg_size)
{
    int req_mem_size    = starting_address - memory_reg_offset + register_count;
    if (!memory_ptr || starting_address < memory_reg_offset || memory_reg_size < req_mem_size)
        return MODBUS_ERROR_INSUFFICIENT_MEMORY_SIZE;

    uint16_t offset     = starting_address - memory_reg_offset;
    const uint16_t *d   = &((const DflwHeader *)validated_response)->word_data;
    uint16_t *m         = (uint16_t *)memory_ptr;
    for (uint8_t i = 0; i < register_count; ++i) {
        m[offset + i]   = be16_to_host(d[i]);
    }

    return MODBUS_SUCCESS;
}

#endif // ENABLE_MODBUS_RTU_CLIENT
