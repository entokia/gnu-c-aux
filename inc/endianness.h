#ifndef GNU_C_AUX_ENDIANNESS_H_
#define GNU_C_AUX_ENDIANNESS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#define byte_swap_const_16(v) \
    ((uint16_t) ( (((v) & 0xFF00u) >> 8) \
                | (((v) & 0x00FFu) << 8) ))

#define byte_swap_const_32(v) \
    ((uint32_t) ( (((v) & 0xFF000000ul) >> 24) \
                | (((v) & 0x00FF0000ul) >>  8) \
                | (((v) & 0x0000FF00ul) <<  8) \
                | (((v) & 0x000000FFul) << 24) ))

#define byte_swap_const_64(v) \
    ((uint64_t) ( (((v) & 0xFF00000000000000ull) >> 56) \
                | (((v) & 0x00FF000000000000ull) >> 40) \
                | (((v) & 0x0000FF0000000000ull) >> 24) \
                | (((v) & 0x000000FF00000000ull) >>  8) \
                | (((v) & 0x00000000FF000000ull) <<  8) \
                | (((v) & 0x0000000000FF0000ull) << 24) \
                | (((v) & 0x000000000000FF00ull) << 40) \
                | (((v) & 0x00000000000000FFull) << 56) ))

#if __GNUC_PREREQ(4, 8)
#define byte_swap_16(v) __builtin_bswap16(v)
#else
#define byte_swap_16(v) byte_swap_const_16(v)
#endif

#if __GNUC_PREREQ(4, 3)
#define byte_swap_32(v) __builtin_bswap32(v)
#else
#define byte_swap_32(v) byte_swap_const_32(v)
#endif

#if __GNUC_PREREQ(4, 3)
#define byte_swap_64(v) __builtin_bswap64(v)
#else
#define byte_swap_64(v) byte_swap_const_64(v)
#endif

#if   defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__)

#define host_to_be16(v) byte_swap_16(v)
#define host_to_le16(v) (uint16_t)(v)
#define be16_to_host(v) byte_swap_16(v)
#define le16_to_host(v) (uint16_t)(v)

#define host_to_be32(v) byte_swap_32(v)
#define host_to_le32(v) (uint32_t)(v)
#define be32_to_host(v) byte_swap_32(v)
#define le32_to_host(v) (uint32_t)(v)

#define host_to_be64(v) byte_swap_64(v)
#define host_to_le64(v) (uint64_t)(v)
#define be64_to_host(v) byte_swap_64(v)
#define le64_to_host(v) (uint64_t)(v)

#elif defined(__BYTE_ORDER__) && (__BYTE_ORDER__ == __ORDER_BIG_ENDIAN__)

#define host_to_be16(v) (uint16_t)(v)
#define host_to_le16(v) byte_swap_16(v)
#define be16_to_host(v) (uint16_t)(v)
#define le16_to_host(v) byte_swap_16(v)

#define host_to_be32(v) (uint32_t)(v)
#define host_to_le32(v) byte_swap_32(v)
#define be32_to_host(v) (uint32_t)(v)
#define le32_to_host(v) byte_swap_32(v)

#define host_to_be64(v) (uint64_t)(v)
#define host_to_le64(v) byte_swap_64(v)
#define be64_to_host(v) (uint64_t)(v)
#define le64_to_host(v) byte_swap_64(v)

#else

#error "Unknown byte order. If your compiler doesn't support __BYTE_ORDER__ macro, define it yourself."

#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif // GNU_C_AUX_ENDIANNESS_H_
