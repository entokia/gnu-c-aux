#ifndef GNU_C_AUX_UNION_TYPES_H_
#define GNU_C_AUX_UNION_TYPES_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

typedef union {
    char        as_char;
    int8_t      as_int8;
    uint8_t     as_uint8        = 0;
} union8_t;

typedef union {
    char        as_char   [2];
    int8_t      as_int8   [2];
    uint8_t     as_uint8  [2];
    int16_t     as_int16;
    uint16_t    as_uint16       = 0;
} union16_t;

typedef union {
    char        as_char   [4];
    int8_t      as_int8   [4];
    uint8_t     as_uint8  [4];
    int16_t     as_int16  [2];
    uint16_t    as_uint16 [2];
    int32_t     as_int32;
    uint32_t    as_uint32       = 0;
} union32_t;

typedef union {
    char        as_char   [8];
    int8_t      as_int8   [8];
    uint8_t     as_uint8  [8];
    int16_t     as_int16  [4];
    uint16_t    as_uint16 [4];
    int32_t     as_int32  [2];
    uint32_t    as_uint32 [2];
    int64_t     as_int64;
    int64_t     as_uint64       = 0;
} union64_t;

typedef union {
    struct {
        uint8_t     b00 : 1;
        uint8_t     b01 : 1;
        uint8_t     b02 : 1;
        uint8_t     b03 : 1;
        uint8_t     b04 : 1;
        uint8_t     b05 : 1;
        uint8_t     b06 : 1;
        uint8_t     b07 : 1;
    } bits;

    uint8_t     value           = 0;
} ubits8_t;

typedef union {
    struct {
        uint16_t    b00 : 1;
        uint16_t    b01 : 1;
        uint16_t    b02 : 1;
        uint16_t    b03 : 1;
        uint16_t    b04 : 1;
        uint16_t    b05 : 1;
        uint16_t    b06 : 1;
        uint16_t    b07 : 1;
        uint16_t    b08 : 1;
        uint16_t    b09 : 1;
        uint16_t    b10 : 1;
        uint16_t    b11 : 1;
        uint16_t    b12 : 1;
        uint16_t    b13 : 1;
        uint16_t    b14 : 1;
        uint16_t    b15 : 1;
    } bits;

    uint16_t     value           = 0;
} ubits16_t;

typedef union {
    struct {
        uint16_t    b00 : 1;
        uint16_t    b01 : 1;
        uint16_t    b02 : 1;
        uint16_t    b03 : 1;
        uint16_t    b04 : 1;
        uint16_t    b05 : 1;
        uint16_t    b06 : 1;
        uint16_t    b07 : 1;
        uint16_t    b08 : 1;
        uint16_t    b09 : 1;
        uint16_t    b10 : 1;
        uint16_t    b11 : 1;
        uint16_t    b12 : 1;
        uint16_t    b13 : 1;
        uint16_t    b14 : 1;
        uint16_t    b15 : 1;
        uint16_t    b16 : 1;
        uint16_t    b17 : 1;
        uint16_t    b18 : 1;
        uint16_t    b19 : 1;
        uint16_t    b20 : 1;
        uint16_t    b21 : 1;
        uint16_t    b22 : 1;
        uint16_t    b23 : 1;
        uint16_t    b24 : 1;
        uint16_t    b25 : 1;
        uint16_t    b26 : 1;
        uint16_t    b27 : 1;
        uint16_t    b28 : 1;
        uint16_t    b29 : 1;
        uint16_t    b30 : 1;
        uint16_t    b31 : 1;
    } bits;

    uint32_t     value           = 0;
} ubits32_t;

#ifdef __cplusplus
}
#endif
    
#endif // GNU_C_AUX_UNION_TYPES_H_
