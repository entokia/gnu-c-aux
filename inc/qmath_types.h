#ifndef GNU_C_AUX_QMATH_TYPES_H_
#define GNU_C_AUX_QMATH_TYPES_H_

#include <stdint.h>

typedef int32_t q6_t;
typedef int32_t q8_t;
typedef int32_t q15_t;
typedef int32_t q16_t;
typedef int32_t q19_t;
typedef int32_t q20_t;
typedef int32_t q24_t;

#ifdef __cplusplus
#define FLOAT_TO_Q6 (v) static_cast<int32_t>((v) * 64.0f)
#define FLOAT_TO_Q8 (v) static_cast<int32_t>((v) * 256.0f)
#define FLOAT_TO_Q15(v) static_cast<int32_t>((v) * 32768.0f)
#define FLOAT_TO_Q16(v) static_cast<int32_t>((v) * 65536.0f)
#define FLOAT_TO_Q19(v) static_cast<int32_t>((v) * 524288.0f)
#define FLOAT_TO_Q20(v) static_cast<int32_t>((v) * 1048576.0f)
#define FLOAT_TO_Q24(v) static_cast<int32_t>((v) * 16777216.0f)

#define Q6_TO_FLOAT (v) static_cast<float>((v) / 64.0f)
#define Q8_TO_FLOAT (v) static_cast<float>((v) / 256.0f)
#define Q15_TO_FLOAT(v) static_cast<float>((v) / 32768.0f)
#define Q16_TO_FLOAT(v) static_cast<float>((v) / 65536.0f)
#define Q19_TO_FLOAT(v) static_cast<float>((v) / 524288.0f)
#define Q20_TO_FLOAT(v) static_cast<float>((v) / 1048576.0f)
#define Q24_TO_FLOAT(v) static_cast<float>((v) / 16777216.0f)
#else
#define FLOAT_TO_Q6 (v) (int32_t) ((v) * 64.0f)
#define FLOAT_TO_Q8 (v) (int32_t) ((v) * 256.0f)
#define FLOAT_TO_Q15(v) (int32_t) ((v) * 32768.0f)
#define FLOAT_TO_Q16(v) (int32_t) ((v) * 65536.0f)
#define FLOAT_TO_Q19(v) (int32_t) ((v) * 524288.0f)
#define FLOAT_TO_Q20(v) (int32_t) ((v) * 1048576.0f)
#define FLOAT_TO_Q24(v) (int32_t) ((v) * 16777216.0f)

#define Q6_TO_FLOAT (v) (float) ((v) / 64.0f)
#define Q8_TO_FLOAT (v) (float) ((v) / 256.0f)
#define Q15_TO_FLOAT(v) (float) ((v) / 32768.0f)
#define Q16_TO_FLOAT(v) (float) ((v) / 65536.0f)
#define Q19_TO_FLOAT(v) (float) ((v) / 524288.0f)
#define Q20_TO_FLOAT(v) (float) ((v) / 1048576.0f)
#define Q24_TO_FLOAT(v) (float) ((v) / 16777216.0f)
#endif // __cplusplus
    
#endif // GNU_C_AUX_QMATH_TYPES_H_
