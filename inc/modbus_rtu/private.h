#ifndef GNU_C_AUX_MODBUS_RTU_PRIVATE_H_
#define GNU_C_AUX_MODBUS_RTU_PRIVATE_H_

#include "modbus_rtu/defines.h"

#ifdef ENABLE_MODBUS_RTU_PRIVATE

#include "modbus_rtu/common.h"

#define MIN_MESSAGE_LEN 5
#define FUNC_MASK       0x7Fu

#ifdef __cplusplus
extern "C" {
#endif

// Request  0x01, 0x02, 0x03, 0x04
// Response 0x0F, 0x10
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint16_t    data_addr;
    uint16_t    data_count;
    uint16_t    checksum;
} DfaaccssPacket;
#pragma pack(pop)

// Request  0x05, 0x06
// Response 0x05, 0x06
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint16_t    data_addr;
    uint16_t    data_value;
    uint16_t    checksum;
} DfaavvssPacket;
#pragma pack(pop)

// Request  0x0F
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint16_t    bit_addr;
    uint16_t    bit_count;
    uint8_t     byte_len;
    uint8_t     byte_data;
} DfaacclbHeader;
#pragma pack(pop)

// Request  0x10
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint16_t    reg_addr;
    uint16_t    reg_count;
    uint8_t     byte_len;
    uint16_t    word_data;
} DfaacclwHeader;
#pragma pack(pop)

// Response 0x01, 0x02
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint8_t     byte_len;
    uint8_t     byte_data;
} DflbHeader;
#pragma pack(pop)

// Response 0x03, 0x04
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint8_t     byte_len;
    uint16_t    word_data;
} DflwHeader;
#pragma pack(pop)

// Response error
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint8_t     exception;
    uint16_t    checksum;
} DfessPacket;
#pragma pack(pop)

// Meta header to access data address and count
#pragma pack(push, 1)
typedef struct {
    uint8_t     dev_addr;
    uint8_t     function;
    uint16_t    data_addr;
    uint16_t    data_count;
} DfaaccHeader;
#pragma pack(pop)

extern const uint16_t   s_ansi_crc16_table[];

int                     multiple_regs_data_length(uint16_t count);
int                     multiple_bits_data_length(uint16_t count);

int                     check_message_buffer_and_calc_length(
                            uint8_t function, uint16_t count,
                            uint8_t *message_buffer, uint8_t buffer_length);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ENABLE_MODBUS_RTU_COMMON

#endif // GNU_C_AUX_MODBUS_RTU_PRIVATE_H_
