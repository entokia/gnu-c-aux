#ifndef GNU_C_AUC_MODBUS_RTU_COMMON_H_
#define GNU_C_AUC_MODBUS_RTU_COMMON_H_

#include "modbus_rtu/defines.h"

#ifdef ENABLE_MODBUS_RTU_COMMON

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

enum ModbusFunction {
    MODBUS_FUNC_UNDEFINED                       = 0x00,
    MODBUS_FUNC_READ_COILS                      = 0x01,
    MODBUS_FUNC_READ_DISCRETE_INPUTS            = 0x02,
    MODBUS_FUNC_READ_HOLDING_REGISTERS          = 0x03,
    MODBUS_FUNC_READ_INPUT_REGISTERS            = 0x04,

    MODBUS_FUNC_WRITE_SINGLE_COIL               = 0x05,
    MODBUS_FUNC_WRITE_SINGLE_REGISTER           = 0x06,

    MODBUS_FUNC_WRITE_MULTIPLE_COILS            = 0x0F,
    MODBUS_FUNC_WRITE_MULTIPLE_REGISTERS        = 0x10,
};

enum ModbusResponseType {
    MODBUS_RESPONSE_NORMAL                      = 0x00,
    MODBUS_RESPONSE_FAULT                       = 0x80,
};

enum ModbusError {
    MODBUS_SUCCESS                              = 0,

    MODBUS_ERROR_UNKNOWN_FUNCTION               = MODBUS_SUCCESS - 1,

    MODBUS_ERROR_INSUFFICIENT_MEMORY_SIZE       = MODBUS_ERROR_UNKNOWN_FUNCTION - 1,
    MODBUS_ERROR_INSUFFICIENT_BUFFER_SIZE       = MODBUS_ERROR_INSUFFICIENT_MEMORY_SIZE - 1,

    MODBUS_ERROR_MESSAGE_TOO_SHORT              = MODBUS_ERROR_INSUFFICIENT_BUFFER_SIZE - 1,
    MODBUS_ERROR_MESSAGE_TOO_LONG               = MODBUS_ERROR_MESSAGE_TOO_SHORT - 1,

    MODBUS_ERROR_WRONG_DEVICE_ADDRESS           = MODBUS_ERROR_MESSAGE_TOO_LONG - 1,
    MODBUS_ERROR_WRONG_FUNCTION                 = MODBUS_ERROR_WRONG_DEVICE_ADDRESS - 1,
    MODBUS_ERROR_WRONG_DATA_ADDRESS             = MODBUS_ERROR_WRONG_FUNCTION - 1,
    MODBUS_ERROR_WRONG_DATA_COUNT               = MODBUS_ERROR_WRONG_DATA_ADDRESS - 1,
    MODBUS_ERROR_WRONG_DATA_VALUE               = MODBUS_ERROR_WRONG_DATA_COUNT - 1,
    MODBUS_ERROR_WRONG_BYTE_COUNT               = MODBUS_ERROR_WRONG_DATA_VALUE - 1,
    MODBUS_ERROR_WRONG_MESSAGE_LENGTH           = MODBUS_ERROR_WRONG_BYTE_COUNT - 1,
    MODBUS_ERROR_WRONG_CHECKSUM                 = MODBUS_ERROR_WRONG_MESSAGE_LENGTH - 1,
};

int             modbus_rtu_request_length (uint8_t function, uint16_t data_count);
int             modbus_rtu_response_length(uint8_t function, uint16_t data_count);

uint16_t        modbus_rtu_reg_address(const void *memory_ptr, const void *reg_ptr);
uint16_t        modbus_rtu_bit_address(const void *memory_ptr, const void *byte_ptr, uint8_t bit_number);

uint8_t         modbus_rtu_get_bit(const uint8_t *memory_ptr, uint16_t memory_bit_offset, uint16_t bit_address);
void            modbus_rtu_set_bit(uint8_t *memory_ptr, uint16_t memory_bit_offset, uint16_t bit_address, uint8_t bit_value);

uint16_t        modbus_rtu_ansi_crc16(const uint8_t *data, uint8_t length, uint8_t swap_bytes);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ENABLE_MODBUS_RTU_COMMON

#endif // GNU_C_AUC_MODBUS_RTU_COMMON_H_
