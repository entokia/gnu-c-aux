#ifndef GNU_C_AUX_MODBUS_RTU_MODBUS_H_
#define GNU_C_AUX_MODBUS_RTU_MODBUS_H_

#include "modbus_rtu/defines.h"

#ifdef ENABLE_MODBUS_RTU_COMMON
#include "modbus_rtu/common.h"
#endif

#ifdef ENABLE_MODBUS_RTU_CLIENT
#include "modbus_rtu/client.h"
#endif

#ifdef ENABLE_MODBUS_RTU_SERVER
#include "modbus_rtu/server.h"
#endif

#endif // GNU_C_AUX_MODBUS_RTU_MODBUS_H_
