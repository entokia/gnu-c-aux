#ifndef GNU_C_AUX_MODBUS_RTU_DEFINES_H_
#define GNU_C_AUX_MODBUS_RTU_DEFINES_H_

#if defined (ENABLE_MODBUS_RTU_CLIENT) || defined (ENABLE_MODBUS_RTU_SERVER)
#ifndef ENABLE_MODBUS_RTU_COMMON
#define ENABLE_MODBUS_RTU_COMMON
#endif
#ifndef ENABLE_MODBUS_RTU_PRIVATE
#define ENABLE_MODBUS_RTU_PRIVATE
#endif
#endif

#endif // GNU_C_AUX_MODBUS_RTU_DEFINES_H_
