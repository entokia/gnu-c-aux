#ifndef GNU_C_AUX_MODBUS_RTU_CLIENT_H_
#define GNU_C_AUX_MODBUS_RTU_CLIENT_H_

#include "modbus_rtu/defines.h"

#ifdef ENABLE_MODBUS_RTU_CLIENT

#include "modbus_rtu/common.h"

#ifdef __cplusplus
extern "C" {
#endif

// Request  0x01
int             modbus_rtu_make_read_coils_request(
                    uint8_t device_address,
                    uint16_t starting_address, uint16_t coil_count,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x02
int             modbus_rtu_make_read_discrete_inputs_request(
                    uint8_t device_address,
                    uint16_t starting_address, uint16_t input_count,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x03
int             modbus_rtu_make_read_holding_registers_request(
                    uint8_t device_address,
                    uint16_t starting_address, uint16_t register_count,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x04
int             modbus_rtu_make_read_input_registers_request(
                    uint8_t device_address,
                    uint16_t starting_address, uint16_t register_count,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x05
int             modbus_rtu_make_write_single_coil_request(
                    uint8_t device_address,
                    uint16_t coil_address, uint16_t coil_value,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x06
int             modbus_rtu_make_write_signle_register_request(
                    uint8_t device_address,
                    uint16_t register_address, uint16_t register_value,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x0F
int             modbus_rtu_make_write_multiple_coils_request(
                    uint8_t device_address,
                    uint16_t starting_address, uint16_t coil_count,
                    const void *memory_ptr, uint16_t memory_bit_offset, uint16_t memory_bit_size,
                    uint8_t *message_buffer, uint8_t buffer_length);

// Request  0x10
int             modbus_rtu_make_write_multiple_registers_request(
                    uint8_t device_address,
                    uint16_t starting_address, uint16_t register_count,
                    const void *memory_ptr, uint16_t memory_reg_offset, uint16_t memory_reg_size,
                    uint8_t *message_buffer, uint8_t buffer_length);

int             modbus_rtu_validate_response(
                    const uint8_t *response, uint8_t response_length,
                    const uint8_t *request,  uint8_t request_length);

int             modbus_rtu_get_fault_response_exception(
                    const uint8_t *validated_response);

int             modbus_rtu_apply_normal_response_bits_to_memory(
                    const uint8_t *validated_response,
                    uint16_t starting_address, uint16_t bit_count,
                    void *memory_ptr, uint16_t memory_bit_offset, uint8_t memory_bit_size);

int             modbus_rtu_apply_normal_response_registers_to_memory(
                    const uint8_t *validated_response,
                    uint16_t starting_address, uint16_t register_count,
                    void *memory_ptr, uint16_t memory_reg_offset, uint8_t memory_reg_size);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // ENABLE_MODBUS_RTU_CLIENT

#endif // GNU_C_AUX_MODBUS_RTU_CLIENT_H_
